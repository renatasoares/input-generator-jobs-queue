#include <bits/stdc++.h>
 
using namespace std;
 
int rand_in_interval(int a, int b) {
        return a + (rand() % (b - a + 1));
}
 
string random_string() {
        const int min_size = 10, max_size = 15;
        int string_size = rand_in_interval(min_size, max_size);
        string s;
        for(int i = 0; i < string_size; ++i)
                s.push_back(rand_in_interval('a', 'z'));
        return s;
}
 
int main() {
        ios_base::sync_with_stdio(false);
        srand(time(NULL));
        int num_skillsets, num_events;
	vector<string> agents_id;
        cin >> num_skillsets >> num_events;
 
        vector<string> skillsets;
        for(int i = 1; i <= num_skillsets; ++i)
                skillsets.push_back(random_string());
 
        cout << "[" << endl;
        while(num_events--) {
                cout << "\t{" << endl;
                if(rand() % 2) {
                        cout << "\t\t\"new_agent\": {" << endl;

 			string id = random_string();
			agents_id.push_back(x);

                        cout << "\t\t\t\"id\": \"" << id << "\"," << endl;
 
                        cout << "\t\t\t\"name\": \"" << random_string() << "\"," << endl;
 
                        cout << "\t\t\t\"primary_skillset\": ";
                        int primary_skillset_size = rand_in_interval(1, 100);
                        cout << "[";
                        for(int i = 1; i <= primary_skillset_size; ++i)
                                cout << "\"" << skillsets[rand() % skillsets.size()] << "\"" << (i == primary_skillset_size ? "" : ", ");
                        cout << "]," << endl;
 
                        cout << "\t\t\t\"secondary_skillset\": ";
                        int secondary_skillset_size = rand_in_interval(0, 100);
                        cout << "[";
                        for(int i = 1; i <= secondary_skillset_size; ++i)
                                cout << "\"" << skillsets[rand() % skillsets.size()] << "\"" << (i == secondary_skillset_size ? "" : ", ");
                        cout << "]" << endl;
 
                        cout << "\t\t}" << endl;
                } else {
                        cout << "\t\t\"new_job\": {" << endl;
                        cout << "\t\t\t\"id\": \"" << random_string() << "\"," << endl;
                        cout << "\t\t\t\"type\": \"" << skillsets[rand() % skillsets.size()] << "\"," << endl;
                        cout << "\t\t\t\"urgent\": " << (rand() % 2 ? "true" : "false") << endl;
                        cout << "\t\t}" << endl;
                }
                cout << "\t}," << endl;
        }

	for(int i=0; i< agents_id.size(); i++){
		cout << "\t{" << endl;
                cout << "\t\t\"job_request\": {" << endl;
                cout << "\t\t\t\"agent_id\": \"" << agents_id[i] << "\"" << endl;
                cout << "\t\t}" << endl;
		cout << "\t}" << (i == agents_id.size()-1 ? "" : ",") << endl;
	}
        cout << "]" << endl;
        return 0;
}
